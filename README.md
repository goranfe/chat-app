#Chat app

A Node.js chat application implemented with Socket.io

#Install and run

Requires Node.js installed.
Clone this repository and run:

```sh
$ npm install
$ node app
```

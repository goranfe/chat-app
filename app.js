const express = require("express");
const path = require("path");
const socket = require("socket.io");
const randomUsername = require("./username");
const timestamp = require("./timestamp");

const app = express();
const port = process.env.PORT || 3000;

//set pug temple engine
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

//set public folder
app.use(express.static(path.join(__dirname, "public")));

//routes
app.get("/", (req, res) => {
  res.render("index");
});

//start app
server = app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

///////////////////////
// socket server code
var io = socket(server);

io.on("connection", socket => {
  let username = randomUsername();
  console.log(`a user: ${username} connected`);

  //send a new client his username
  socket.emit("username", { data: username });

  socket.on("chat", data => {
    let time = timestamp();
    io.sockets.emit("chat", data, time);
  });
});

//make connection
let socket = io.connect();
//defaults to trying to connect to the host that serves the page

let user = '';

//query DOM
let button = document.getElementById('send');
let name = document.getElementById('username');
let output = document.getElementById('output');
let message = document.getElementById('message');
let chatWindow = document.getElementById('chat-window');

//emit events
button.addEventListener('click', function() {
    if(message.value != "") {
        socket.emit('chat', {  //send an object
            message: message.value,
            user: user
        });
        message.value = ""; //reset field
    }
    //give textfield focus back after enter
    message.focus();
});

message.addEventListener('keyup', function(event) {
    if(event.keyCode === 13) {
        button.click();
    }
});


//listen for events
socket.on('chat', (data, time) => {
    output.innerHTML += `<p><span class='timestamp'>${time}</span>
        <strong class='user'>${data.user}:</strong><br />
        ${data.message}</p>`;

    //scrolling when content overflows
    chatWindow.scrollTo(0, output.scrollHeight);
});

socket.on('username', data => {
    user = data.data;
    name.innerHTML = user;
});

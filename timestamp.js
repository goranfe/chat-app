function timestamp() {
    let d = new Date();
    let h = d.getHours() < 10 ? '0'+d.getHours() : d.getHours();
    let m = d.getMinutes() < 10 ? '0'+d.getMinutes() : d.getMinutes();
    let s = d.getSeconds() < 10 ? '0'+d.getSeconds() : d.getSeconds();
    return `[${h}:${m}:${s}]`;
}

module.exports = timestamp;

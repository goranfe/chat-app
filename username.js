const prefix = 'user-';

//random four ditig number
function randomUsername () {
    let digits = Math.floor(Math.random()*9000) + 1000;
    return prefix + digits;
}

module.exports = randomUsername;
